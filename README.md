# EpicSw.app Interface

[![Styled With Prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://prettier.io/)

An open source interface for EpicSw.app -- a protocol for decentralized exchange of Ethereum tokens.

## Listing a token

Please see the
[@epicswapp/default-token-list](https://gitlab.com/epicswapp/default-token-list)
repository.

## Development

### Install Dependencies

```bash
yarn
```

### Run

```bash
yarn start
```


## Contributions

**Please open all pull requests against the `master` branch.**

